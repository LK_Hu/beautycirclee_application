module JsonHelpers
  def json(json_string)
    @json ||= JSON.parse(json_string, symbolize_names: true)
  end
end