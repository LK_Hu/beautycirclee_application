module EngineControllerPatch
  def self.included(base)
    @http_methods ||= [ :get, :post, :put, :patch, :delete ]
    @http_methods.each { |http_method| patch_http_method http_method }
  end
 
  def self.patch_http_method(http_method)
    define_method(http_method) do |action, parameters = nil, session = nil, flash = nil|
      process_action(action, parameters, session, flash, http_method.to_s.upcase)
    end
  end
 
  private
 
  def process_action(action, parameters = nil, session = nil, flash = nil, method = "GET")
    parameters ||= {}
    process(action, method, parameters.merge!(:use_route => :my_engine_name), session, flash, )
  end
end