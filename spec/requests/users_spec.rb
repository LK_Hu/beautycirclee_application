require 'rails_helper'

RSpec.describe API::V1::UsersController, :type => :request do
  before {
    @domain = SUBDOMAIN_API_V1
  }

  let(:valid_attributes) {
    { user_name: 'vhlk', email: 'hu.likun@gmail.com', password: 'hijklmn', password_confirmation: 'hijklmn'}
  }

  let(:invalid_attributes) {
    { user_name: 'vhlk', email: 'hu likun@gmail.com', password: 'opqrst', password_confirmation: 'opqrst'}
  }

  describe "GET /users.json (controller action: /users#index)" do
    it "works! (now write some real specs)" do
      # get "#{@domain}/users.json"
      get "#{@domain}/users", {}, {'Accept' => Mime::JSON}
      expect(response.status).to be(200)
      expect(response.content_type).to be(Mime::JSON)
      expect(json(response.body)[:users].size).to eq(User.count)
      # raise response.body
    end
  end

  describe "GET /users/:id (/users#show)" do
    it "assigns all users as @users" do
      user = User.create! valid_attributes
      get "#{@domain}/users/#{user.id}", {}, {'Accept' => Mime::JSON}#, valid_session
      expect(response.status).to be(200)
      expect(response.content_type).to be(Mime::JSON)
      expect(json(response.body)[:user][:id]).to eq(user.id)
    end
  end

  describe "POST /users (/users#create)" do
    it "with valid params" do
      post "#{@domain}/users",
      {
        user:
        {
          user_name: "LikunTest",
          email: "likuntest@test.com",
          password: 'hhhhhhh',
          password_confirmation: 'hhhhhhh'
        }
      },
      {
        Accept: Mime::JSON, content_type: Mime::JSON.to_s
      }
      expect(response.status).to eq(201)
    end
  end

  describe "PATCH/PUT /users/:id (/users#update)" do
    let(:new_attributes) {
      { user_name: 'vhlk_new', email: 'hu.likun_new@gmail.com', password: 'er98hdnva', password_confirmation: 'er98hdnva' }
    }

    it "with valid params" do
      user = User.create! valid_attributes
      put "#{@domain}/users/#{user.id}",
      {
        id: user.id,
        user: new_attributes
      },
      {
        Accept: Mime::JSON, content_type: Mime::JSON.to_s
      }
      user.reload
      expect(response.status).to eq(204)
      expect(user[:user_name]).to eq(new_attributes[:user_name])
      expect(user[:email]).to eq(new_attributes[:email])
    end

    it "with invalid params" do
      user = User.create! valid_attributes
      put "#{@domain}/users/#{user.id}",
      {
        id: user.id,
        user: invalid_attributes
      },
      {
        Accept: Mime::JSON, content_type: Mime::JSON.to_s
      }
      user.reload
      expect(user.user_name).to eq(valid_attributes[:user_name])
      expect(user.email).to eq(valid_attributes[:email])
    end
  end

  describe "DELETE /users/:id (/users#destroy)" do
    it "destroys the requested user" do
      user = User.create! valid_attributes
      delete "#{@domain}/users/#{user.id}",
      {
        id: user.id
      },
      { Accept: Mime::JSON, content_type: Mime::JSON.to_s }
      expect(response.status).to eq(204)
    end
  end

end
