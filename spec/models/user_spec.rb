# == Schema Information
#
# Table name: users
#
#  id         :integer          not null, primary key
#  userName   :string(255)
#  email      :string(255)
#  created_at :datetime
#  updated_at :datetime
#

require 'rails_helper'

RSpec.describe User, :type => :model do
  before {
    @user = User.new(user_name: 'lkh', email: 'lkh@test.com', password: 'helloworld', password_confirmation: 'helloworld')
  }

  subject { @user }

  it { should respond_to(:user_name) }
  it { should respond_to(:email) }
  it { should respond_to(:password_digest)}
  it { should respond_to(:password) }
  it { should respond_to(:password_confirmation) }
  it { should respond_to(:authenticate) }
  it { should be_valid }

  describe "when password is not present" do
    before {
      @user.password = @user.password_confirmation = ' '
    }
    it { should_not be_valid }
  end

  describe "when password doesn't match" do
    before {
      @user.password_confirmation = "mismatch"
    }
    it { should_not be_valid }
  end

  describe "when password conformation is nil" do
    before {
      @user.password_confirmation = nil
    }
    it { should_not be_valid }
  end

  describe "return value of authenticated value" do
    before {
      @user.save
    }
    let(:found_user) { User.find_by_email(@user.email).authenticate(@user.password) }
    it { should == found_user }
  end

  describe "with invalid password" do
    before {
      @user.save
    }
    let(:user_for_invalid_password) { User.find_by_email(@user.email).authenticate('invalidpassword') }
    it { should_not == user_for_invalid_password }

  end

end








