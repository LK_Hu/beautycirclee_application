module API
  module V1
    class UsersController < ApplicationController
      before_action :set_user, only: [:show, :update, :destroy]

      # GET /users
      # GET /users.json
      def index
        @users = User.all

        respond_to do |format|
          format.json { render json: @users, status: 200 }
        end
      end

      # GET /users/1
      # GET /users/1.json
      def show
        respond_to do |format|
          format.json { render json: @user, status: 200 }
        end
      end

      # POST /users
      # POST /users.json
      def create
        @user = User.new(user_params)

        respond_to do |format|
          if @user.save
            format.json { render json: @user, status: :created }
          else
            format.json { render json: @user.errors, status: :unprocessable_entity }
          end
        end
      end

      # PATCH/PUT /users/1
      # PATCH/PUT /users/1.json
      def update
        respond_to do |format|
          if @user.update(user_params)
            format.json { head :no_content }
          else
            format.json { render json: @user.errors, status: :unprocessable_entity }
          end
        end
      end

      # DELETE /users/1
      # DELETE /users/1.json
      def destroy
        @user.destroy
        respond_to do |format|
          format.json { head :no_content }
        end
      end

      private
        # Use callbacks to share common setup or constraints between actions.
        def set_user
          @user = User.find(params[:id])
        end

        # Never trust parameters from the scary internet, only allow the white list through.
        def user_params
          params.require(:user).permit(:user_name, :email, :password, :password_confirmation)
        end
    end
  end
end