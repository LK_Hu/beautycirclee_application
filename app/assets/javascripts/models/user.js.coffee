# for more details see: http://emberjs.com/guides/models/defining-models/

BeautycircleeApplication.User = DS.Model.extend
  user_name: DS.attr 'string'
  email: DS.attr 'string'
