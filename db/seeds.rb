# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.create!(user_name: 'likunhu1', email: 'likunhu1@test.com', password: 'helloworld', password_confirmation: 'helloworld')
User.create!(user_name: 'likunhu2', email: 'likunhu2@test.com', password: 'abcdefg', password_confirmation: 'abcdefg')