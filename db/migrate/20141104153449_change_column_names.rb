class ChangeColumnNames < ActiveRecord::Migration
  def change
    rename_column :users, :userName, :user_name
  end
end
